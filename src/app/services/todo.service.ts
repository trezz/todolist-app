// by generating services we have this injectable which allow us to inject into the constuctor
// the service file will help us deal with http models
import { Injectable } from '@angular/core';
// we import http and in other to use it we inject it into the constructor
import { HttpClient, HttpHeaders } from '@angular/common/http';

// we import rxjs which contains fonctions as is observable
import { Observable } from 'rxjs';

import { Todo } from '../models/Todo';


// here then we create httpOptions which is for sending content-type
const httpOptions = {
  headers: new HttpHeaders({
    // here we put in what ever headers we want to send
    'Content-Type': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todosUrl: string = 'https://jsonplaceholder.typicode.com/todos';
  todosLimit: string = '?_limit=10';

  constructor(private http: HttpClient) { }

  // here is the method we created to get Todos from server --- it's a get request
  getTodos(): Observable<Todo[]> {
    // we for a second got hard code it before we get it from http --- and export to be import in the todolist component
    // here then we return an observable
    return this.http.get<Todo[]>(`${this.todosUrl}${this.todosLimit}`);
  }

  // this is a method we created to toggle todos (update todos on server) --- it's a put request which is actually when you
  // update datas on the server
  // in the observable we not gone put anything in the tags because is not gone format as an exact todo since it has the user id
  toggleCompleted(todo: Todo): Observable<any> {
    const url = `${this.todosUrl}/${todo.id}`;
    return this.http.put(url, todo, httpOptions);
  }

  // now we implement the deleteTodo functionality to delete the todo on  the server
  deleteTodo(todo: Todo): Observable<Todo> {
    const url = `${this.todosUrl}/${todo.id}`;
    return this.http.delete<Todo>(url, httpOptions);
  }

  addTodo(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(this.todosUrl, todo, httpOptions);
  }
}
