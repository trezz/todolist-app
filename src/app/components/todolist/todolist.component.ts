import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service';

import { Todo } from '../../models/Todo';


@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {
  todos: Todo[];

  // constructor() { }
  // we now have to get the datas from service and as we said we get it in the constructor
  // we initiate it as private or other public if you but is like to use private so it will work here only)
  // and then assign it to variable which will take the todos form service
  // we can use it with this.todoService.whatever method is in the service
  constructor(private todoService: TodoService) {
    // notice that the this.todos refere to the todos initiated above
    // after adding and return the observable we no longer can do this.todos = this.todoService.getTodos()
    // since it's observable is async
    // ***this.todos = this.todoService.getTodos();
  }

  ngOnInit() {
    // we use now the observable by subscribing to it which you should take it as the .then in async functions
    this.todoService.getTodos().subscribe(todos => {
      this.todos = todos;
    });
  }

  // here we call the other deleteTodo function we created in the html file and add in some actions
  deleteTodo(todo: Todo) {
    // now we gonna use the methode filter to kindlike delete the todo --- actually the trick is we filter the todos to return
    // the todos that are not the one we have --- explaination in code --- basically we are not deleting it in on the server
    this.todos = this.todos.filter(t => t.id !== todo.id);

    // now we delete it on the server --- ps we can insert the above filter trick in the subscribe but we don't because it's
    // kindlike tricks and makes things faster by deleting it from the UI without waiting the response from server
    this.todoService.deleteTodo(todo).subscribe();
  }

  addTodo(todo: Todo) {
    this.todoService.addTodo(todo).subscribe(to => {
      this.todos.push(to);
    });
  }

}
