// we import all Input to be able to pass data as property
// EventEmitter and Output are here brought in because of the delete event --- concept to put on hold for further explaination
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

// now we import the service inject it in the constructor so that we can use it in the onToggle method
import { TodoService } from '../../services/todo.service';

import { Todo } from 'src/app/models/Todo';


@Component({
  selector: 'app-todos-item',
  templateUrl: './todos-item.component.html',
  styleUrls: ['./todos-item.component.css']
})
export class TodosItemComponent implements OnInit {
  // we create the input property and pass in todo --- is actually for when we are taking in something
  @Input() todo: Todo;

  //  we create the output now to emit something out to the parent component --- next we catch it in the parent html element
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private todoservice: TodoService) { }

  ngOnInit() {
  }


  // set dynamic classes
  setClasses() {
    const classes = {
      todo: true,
      'is-completed' : this.todo.completed
    };
    return classes;
  }

  onToggle(todo) {
    // notice that here we have directly access to todo without mentioning this because we already have it in fonction argument
    // toggle in the UI
    todo.completed = !todo.completed;

    // Toggle in the server
    this.todoservice.toggleCompleted(todo).subscribe(t => {
      console.log(t);
    });
  }
  onDelete(todo) {
    // here we call the emit function to "emit" --- wait further explaination
    this.deleteTodo.emit(todo);
  }
}
