export class Todo {
	// tslint:disable-next-line:indent
	id: number;
	title: string;
	completed: boolean;
}
