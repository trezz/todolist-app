import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // ******commented code for learnings ******************************

  // // here we use typescript which is what recommanded and that makes our code more robust
  // /* if we have initiated the type with string and assign it with a number or something else which is not a string it will run into
  // error and  ex (name: string = 12;)*/
  // name: string = 'van';

  // // we can also use methods and functions here like constructor which as we know in oriented object run at app starting
  // constructor() {
  //   // we have access here to name proprety and we can change it at app starter which can make the first value change
  //   // ****this.name = 'Goethe';

  //   // and as we created a method below we can call it to change value of name
  //   this.changeName('VanGoethe');
  // }

  // // changeName(arg:type):void {} we can use :void which means that we don't want our method to return anything
  // changeName(name: string): void {
  //   this.name = name;
  // }


  // *****************************end********************************************

}
