import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// we need to import all component we want to route to
import { TodolistComponent } from './components/todolist/todolist.component';
import { AboutComponent } from './components/pages/about/about.component';

const routes: Routes = [
  { path: '', component: TodolistComponent },
  { path: 'about', component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
